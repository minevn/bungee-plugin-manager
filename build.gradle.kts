plugins {
    id("java")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven {
        setUrl("http://pack.minevn.net/repo")
        isAllowInsecureProtocol = true
    }
}

dependencies {
    compileOnly("minevn.depend:BungeeCord:1.19")
}

tasks {
    val jarName = "BungeePluginManager"
    var originalName = ""
    val path = project.properties["shadowPath"]

    jar {
        originalName = archiveFileName.get()
    }

    register("customCopy") {
        dependsOn(jar)

        doLast {
            if (path != null) {
                println("Copying $originalName to $path")
                val to = File("$path/$originalName")
                val rename = File("$path/$jarName.jar")
                File(project.projectDir, "build/libs/$originalName").copyTo(to, true)
                if (rename.exists()) rename.delete()
                to.renameTo(rename)
                println("Copied")
            }
        }
    }


    assemble {
        dependsOn(get("customCopy"))
    }
}
